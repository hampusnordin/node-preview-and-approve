module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*!\n * <%= pkg.name %> <%= pkg.version %>\n *\n * <%= pkg.description %>\n *\n * Source: <%= grunt.task.current.filesSrc %>\n *\n * Author: <%= pkg.author %>\n * Last built: <%= grunt.template.today("yyyy-mm-dd") %>\n */\n'
      },
      build: {
        files: [{
          expand: true,
          cwd: 'assets/js/source/',
          src: '*.js',
          dest: 'assets/js/build/',
          ext: '.js',
          extDot: 2
        }]
      }
    },
    sass: {
      dist: {
        options: {
          require: ['./ruby/timestamp.rb'],
          style: "compressed"
        },
        files: {
          'assets/css/npaa.admin.css': 'assets/sass/npaa.admin.scss'
        }
      }
    },
    watch: {
      css: {
        files: 'assets/sass/**/*.scss',
        tasks: ['sass']
      },
      js: {
        files: 'assets/js/source/**/*.js',
        tasks: ['uglify']
      }
    }
  });

  // Uglify
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.registerTask('default', ['uglify']);

  // Sass
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default',['watch']);
};
