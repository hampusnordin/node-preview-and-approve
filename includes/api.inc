<?php

/**
 * @file
 * Contains all NPAA Api functions.
 */

/**
 * The variable name which is used to store the 
 * module settings in the variable table with 
 * variable_get() and variable_set().
 **/
define('NPAA_SETTINGS_KEY', 'npaa_settings');

/**
 * Status ID for the "review" status.
 **/
define('NPAA_STATUS_REVIEW', 1);

/**
 * Status ID for the status "needs work".
 **/
define('NPAA_STATUS_NEEDS_WORK', 2);

/**
 * Status ID for the status "approved".
 **/
define('NPAA_STATUS_APPROVED', 3);

/**
 * Returns the default module settings.
 * 
 * @param  $full_build Boolean value which determines if the complete structure should be built or not.
 *   Defaults to false.
 *
 * @return array
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_settings_defaults($full_build = FALSE) {
  $defaults = array(
    'approve_link_in_drupal_messages' => 1,
    'activated_node_types' => array(),
    'node_types' => array(),
    'statuses' => array(
      'publish_on_update' => array(NPAA_STATUS_APPROVED),
      'delete_preview_on_publish' => 1,
    ),
  );
  if ($full_build === TRUE) {
    $node_types = node_type_get_types();
    foreach ($node_types as $type => $data) {
      $defaults['node_types'][$type] = array(
        'activated' => 0,
        'default_preview_mail_send_by_default' => 0,
      );
    }
  }
  return $defaults;
}

/**
 * Returns various table mapping settings.
 *
 * @return array
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_entity_table_mappings() {
  return array(
    'NpaaNode' => array(
      'table' => 'npaa_nodes',
      'id_key' => 'nid',
      'primary_keys' => array('nid'),
    ),
    'NpaaStatus' => array(
      'table' => 'npaa_statuses',
      'id_key' => 'sid',
      'primary_keys' => array('sid'),
    ),
  );
}

/**
 * Gets the stored settings merged with the defaults.
 * 
 * @param boolean $reset If a hard static reset should be done or not.
 *   Defaults to true.
 *
 * @return array
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_settings_get($reset = FALSE) {
  $npaa_settings = &drupal_static(__FUNCTION__);
  if (empty($npaa_settings) || $reset === TRUE) {
    $stored_settings = variable_get(NPAA_SETTINGS_KEY, array());
    $npaa_settings = drupal_array_merge_deep(npaa_settings_defaults(), $stored_settings);
  }
  return $npaa_settings;
}

/**
 * Sets the module settings.
 * 
 * @param Array $new_settings An array with the new settings.
 *   Defaults to empty array.
 *
 * @return void
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_settings_set(Array $new_settings = array()) {
  // Get the stored settings and force a reset.
  $stored_settings = npaa_settings_get(TRUE);
  // To prevent duplicates and already removed statuses from being 
  // stored continuously.
  if (! empty($new_settings['statuses']['publish_on_update'])) {
    $stored_settings['statuses']['publish_on_update'] = array();
  }
  // Merge the new settings into the stored settings.
  $merged_settings = drupal_array_merge_deep($stored_settings, $new_settings);
  // ...
  $merged_settings['activated_node_types'] = array();
  foreach ($merged_settings['node_types'] as $type => $type_values) {
    if ($type_values['activated'] === 1) {
      $merged_settings['activated_node_types'][] = $type;
    }
  }
  // Save/persist the settings.
  variable_set(NPAA_SETTINGS_KEY, $merged_settings);
}

/**
 * Persists an Npaa entity.
 * 
 * @param NpaaEntityInterface $entity The entity to persist.
 * @param Array               $fields Which fields to persist to the database.
 *
 * @return bool
 *   True if entity was created. False otherwise.
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_create_entity(NpaaEntityInterface $entity, Array $fields = array()) {
  $table_mappings = npaa_entity_table_mappings();
  if (isset($table_mappings[get_class($entity)])) {
    $table_mappings = $table_mappings[get_class($entity)];
  } else {
    // Has no table mappings. Really wierd.
    return false;
  }
  $entityFields = $entity->fields();
  if (! empty($fields)) {
    $entityFields = array_intersect_key($entityFields, array_flip($fields));
  }
  $object = (object) $entityFields;
  return drupal_write_record($table_mappings['table'], $object) === SAVED_NEW;
}

/**
 * Updates an Npaa entity.
 * 
 * @param NpaaEntityInterface $entity The entity to update.
 * @param Array               $fields Which fields to persist to the database.
 *
 * @return bool
 *   True if entity was updated. False otherwise.
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_update_entity(NpaaEntityInterface $entity, Array $fields = array()) {
  $table_mappings = npaa_entity_table_mappings();
  if (isset($table_mappings[get_class($entity)])) {
    $table_mappings = $table_mappings[get_class($entity)];
  } else {
    // Has no table mappings. Really wierd.
    return false;
  }
  $entityFields = $entity->fields();
  if (! empty($fields)) {
    $entityFields = array_intersect_key($entityFields, array_flip($fields));
  }
  $object = (object) $entityFields;
  return drupal_write_record($table_mappings['table'], $object, $table_mappings['primary_keys']) === SAVED_UPDATED;
}

/**
 * Delete an Npaa entity.
 * 
 * @param NpaaEntityInterface $entity The entity to delete.
 *
 * @return bool
 *   Returns true if exactly one entity was deleted. False otherwise.
 * @author Hampus Nordin <nordin.hampus@bolt.se>
 **/
function npaa_delete_entity(NpaaEntityInterface $entity) {
  $table_mappings = npaa_entity_table_mappings();
  if (isset($table_mappings[get_class($entity)])) {
    $table_mappings = $table_mappings[get_class($entity)];
  } else {
    // Has no table mappings. Really wierd.
    return false;
  }
  $num_deleted = db_delete($table_mappings['table'])
    ->condition($table_mappings['id_key'], $entity->getId())
    ->execute();
  return $num_deleted === 1;
}

/**
 * Loads multiple Npaa entities by their ids.
 * 
 * @param string $entity_type The class name of the entity to be loaded.
 * @param array  $ids         An array of ids to load.
 *   Pass an array with only the value 'all' to load all entities. This 
 *   will always trigger a reset.
 *
 * @return Array
 *   Returns an array of entities as assoc arrays.
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_load_entities($entity_type, Array $ids, $reset = FALSE) {
  $entities = &drupal_static(__FUNCTION__);
  $load_all = reset($ids) === 'all';
  if (! class_exists($entity_type)) {
    // Not a valid class.
    return array();
  }
  $table_mappings = npaa_entity_table_mappings();
  if (isset($table_mappings[$entity_type])) {
    $table_mappings = $table_mappings[$entity_type];
  } else {
    // Has no table mappings. Really wierd.
    return array();
  }
  // Only query database if not every id has already been loaded.
  if ($reset === TRUE || $load_all === TRUE || empty($entities[$entity_type]) || ! _npaa_all_array_keys_exist($ids, $entities[$entity_type])) {
    // Build select query.
    $query = db_select($table_mappings['table'], 'e')
      ->fields('e');
    // 
    if ($load_all === TRUE) {
      $ids = array();
    } else {
      // Filter out ids which are already cached.
      $cond_ids = ($reset === TRUE || empty($entities[$entity_type])) ? $ids : _npaa_filter_existing_ids($ids, $entities[$entity_type]);
      $query->condition('e.' . $table_mappings['id_key'], $cond_ids, 'IN');
    }
    // Get all entities as assoc arrays.
    $result = $query->execute()->fetchAllAssoc($table_mappings['id_key'], PDO::FETCH_ASSOC);
    // Save entities in drupal static cache.
    foreach ($result as $id => $entity) {
      $entities[$entity_type][$id] = $entity;
    }
    if (empty($entities[$entity_type])) {
      $entities[$entity_type] = array();
    }
  }
  return  empty($ids) ? $entities[$entity_type] : array_intersect_key($entities[$entity_type], array_flip($ids));
}

/**
 * Persist an Npaa status.
 *
 * @param NpaaStatus $status The status entity to persist.
 * 
 * @return boolean
 * @see npaa_create_entity()
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_create_status(NpaaStatus $status) {
  return npaa_create_entity($status, array('title', 'description', 'publicly_selectable'));
}

/**
 * Persist an Npaa node.
 *
 * @param NpaaNode $node The node entity to persist.
 * 
 * @return boolean
 * @see npaa_create_entity()
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_create_node(NpaaNode $node) {
  return npaa_create_entity($node);
}

/**
 * Updates an Npaa node.
 *
 * @param NpaaNode $node The node entity to be updated.
 * 
 * @return boolean
 * @see npaa_update_entity()
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_update_node(NpaaNode $node) {
  return npaa_update_entity($node);
}

/**
 * Deletes an Npaa node.
 * 
 * @param NpaaNode $node The node entity to be deleted.
 *
 * @return boolean
 * @see  npaa_delete_entity()
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_delete_node(NpaaNode $node) {
  return npaa_delete_entity($node);
}

/**
 * Load multiple Npaa nodes.
 * 
 * @param array $nids  An array of nids to load.
 * @param bool  $reset Forces a database query to reload nodes.
 *
 * @return array
 *   Returns an array of NpaaNode objects.
 * @see npaa_load_entities()
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_load_nodes(Array $nids, $reset = FALSE) {
  $entities = npaa_load_entities('NpaaNode', $nids, $reset);
  $nodes = array();
  foreach ($entities as $data) {
    $node = new NpaaNode();
    $node->setId($data['nid'])
      ->setPreviewHash($data['preview_hash'])
      ->setSid($data['sid'])
      ->setStatusUpdateCode($data['status_update_code']);
    $nodes[$node->getId()] = $node;
  }
  return $nodes;
}

/**
 * Loads a single Npaa node.
 *
 * @param int     $nid   The id to load.
 * @param boolean $reset Forces a database query to reload the node.
 * 
 * @return NpaaNode | boolean
 *   Returns a single NpaaNode object or false if the nid is not found.
 * @see npaa_load_nodes()
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_load_node($nid, $reset = FALSE) {
  $node = npaa_load_nodes(array($nid), $reset);
  return $node ? reset($node) : FALSE;
}

/**
 * Load multiple Npaa statuses.
 * 
 * @param array $sids  An array of sids to load.
 * @param bool  $reset Forces a database query to reload statuses.
 *
 * @return array
 *   Returns an array of NpaaStatus objects.
 * @see npaa_load_entities()
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_load_statuses(Array $sids, $reset = FALSE) {
  $entities = npaa_load_entities('NpaaStatus', $sids, $reset);
  $statuses = array();
  foreach ($entities as $data) {
    $status = new NpaaStatus();
    $status->setId($data['sid'])
      ->setTitle($data['title'])
      ->setDescription($data['description'])
      ->setPubliclySelectable($data['publicly_selectable']);
    $statuses[$status->getId()] = $status;
  }
  return $statuses;
}

/**
 * Load all status entities.
 *
 * @return Array
 *   Returns an array of NpaaStatus entities.
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_load_all_statuses() {
  return npaa_load_statuses(array('all'));
}

/**
 * Loads a single Npaa status.
 *
 * @param int     $sid   The id to load.
 * @param boolean $reset Forces a database query to reload the status.
 * 
 * @return NpaaStatus | boolean
 *   Returns a single NpaaStatus object or false if the sid is not found.
 * @see npaa_load_statuses()
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_load_status($sid, $reset = FALSE) {
  $status = npaa_load_statuses(array($sid), $reset);
  return $status ? reset($status) : FALSE;
}

/**
 * API function to print the approve link.
 * 
 * @param int     $nid       The node id.
 * @param string  $link_text The link text.
 *   Defaults to t('approve preview').
 * @param boolean $print     If the link should be printed or not.
 *   Defaults to true.
 *
 * @return String
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_approve_link($nid, $link_text = '', $print = true) {
  $npaa_node = npaa_load_node($nid);
  $link_text = $link_text ?: t('approve preview');
  $link = '';
  if ($npaa_node) {
    $link = l($link_text, _npaa_approve_url_structure($nid), array('attributes' => array('class' => array('npaa-approve-link')), 'query' => array('preview' => $npaa_node->getPreviewHash())));
    if ($print) {
        echo $link;
    }
  }
  return $link;
}

/**
 * API function to print the url to the approve node page.
 * 
 * @param int     $nid      The node id.
 * @param boolean $absolute If the link should be absolute (with protocol and domain).
 *   Defaults to false.
 * @param boolean $print    If the link should be printed or not.
 *   Defaults to true.
 *
 * @return string
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_approve_url($nid, $absolute = false, $print = true) {
  $npaa_node = npaa_load_node($nid);
  $url = '';
  if ($npaa_node) {
    $url = url(_npaa_approve_url_structure($nid), array('absolute' => $absolute, 'query' => array('preview' => $npaa_node->getPreviewHash())));
    if ($print) {
      echo $url;
    }
  }
  return $url;
}

/**
 * API function to print the url to the preview node page.
 * 
 * @param int     $nid      The node id.
 * @param boolean $absolute If the link should be absolute (with protocol and domain).
 *   Defaults to false.
 * @param boolean $print    If the link should be printed or not.
 *   Defaults to true.
 *
 * @return string
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_preview_url($nid, $absolute = false, $print = true) {
  $npaa_node = npaa_load_node($nid);
  $url = '';
  if ($npaa_node) {
    $url = url("node/$nid", array('absolute' => $absolute, 'query' => array('preview' => $npaa_node->getPreviewHash())));
    if ($print) {
      echo $url;
    }
  }
  return $url;
}

/**
 * Helper function which builds the approve url.
 * Does not include the required query param!
 *
 * @return String
 * @see npaa_approve_link()
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function _npaa_approve_url_structure($nid) {
  return "node/$nid/npaa-approve";
}

/**
 * Checks if all array keys exist in an array.
 * 
 * @param array $keys  The keys to look for.
 * @param array $array The array to search in.
 *
 * @return boolean
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function _npaa_all_array_keys_exist(Array $keys, Array $array) {
  return count(array_intersect_key(array_flip($keys), $array)) === count($keys);
}

/**
 * Returns an array with the ids which does not exist in the $data array.
 *
 * @param array $ids  Array of ids.
 * @param array $data Array with data to filter with.
 * 
 * @return array
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function _npaa_filter_existing_ids(Array $ids, Array $data) {
  return array_keys(array_diff_key(array_flip($ids), $data));
}
