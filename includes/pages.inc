<?php

/**
 * @file
 * Contains panels related hooks and functions.
 */

/**
 * undocumented function
 *
 * @return void
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_node_page_approve($node) {
    $query_vars = drupal_get_query_parameters();
    $preview = isset($query_vars['preview']) ? trim($query_vars['preview']) : '';

    $page = array(
        '#show_messages' => TRUE,
        '#theme' => 'page',
        '#type' => 'page',
    );
    $page['content'] = array();
    $page['content']['title'] = array(
        '#markup' => '<h1>' . $node->title . '</h1>',
    );
    $page['content']['back_to_preview_link'] = array(
        '#theme' => 'link',
        '#text' => t('Go back to the preview'),
        '#path' => 'node/' . $node->nid,
        '#options' => array(
            'attributes' => array(
                'class' => array('npaa-btn'),
            ),
            'query' => array('preview' => $preview),
            'html' => false,    
        ),
    );
    $page['content']['form'] = drupal_get_form('npaa_node_approve_form', $node);
    return $page;
}
