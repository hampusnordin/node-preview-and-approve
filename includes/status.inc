<?php

/**
 * @file
 * Contains the NpaaStatus class.
 */

/**
 * Class for the NPAA Status.
 *
 * @package default
 * @author 
 **/
class NpaaStatus implements NpaaEntityInterface
{
  private $_id = 0;

  private $_title = '';

  private $_description = '';

  private $_publicly_selectable = FALSE;

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function __construct() {
    return $this;
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function getId() {
    return intval($this->_id);
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function setId($id) {
    $this->_id = intval($id);
    return $this;
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function getTitle() {
    return trim($this->_title);
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function setTitle($title) {
    $this->_title = trim($title);
    return $this;
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function getDescription() {
    return trim($this->_description);
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function setDescription($description) {
    $this->_description = trim($description);
    return $this;
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function getPubliclySelectable() {
    return (bool) $this->_publicly_selectable;
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function setPubliclySelectable($publicly_selectable) {
    $this->_publicly_selectable = (bool) $publicly_selectable;
    return $this;
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function fields() {
    return array(
      'sid' => $this->getId(),
      'title' => $this->getTitle(),
      'description' => $this->getDescription(),
      'publicly_selectable' => (int) $this->getPubliclySelectable(),
    );
  }
} // END class NpaaStatus
