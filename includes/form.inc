<?php

/**
 * @file
 * Contains FAPI forms.
 */

/**
 * Implements hook_form_alter().
 *
 * @return void
 * @see _npaa_node_form_validate(), _npaa_node_form_submit()
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_form_alter(&$form, &$form_state, $form_id = '') {
  // Make sure it's a node form.
  if (! empty($form['#node_edit_form'])) {
    $type = substr($form_id, 0, -strlen('_node_form'));
    $npaa_settings = npaa_settings_get();
    // Check if NPAA is activated for the specific node type.
    if (in_array($type, $npaa_settings['activated_node_types']) && user_access('npaa set node preview')) {
      // Get all NPAA Statuses for the select list.
      $npaa_statuses = npaa_load_all_statuses();
      $status_options = array();
      foreach ($npaa_statuses as $npaa_status) {
        $status_options[$npaa_status->getId()] = $npaa_status->getTitle();
      }
      // Load NpaaNode if a node id exist.
      if (! empty($form['nid']['#value'])) {
        $npaa_node = npaa_load_node(intval($form['nid']['#value']));
      } else {
        $npaa_node = FALSE;
      }
      // Add callbacks 
      $form['#submit'][] = '_npaa_node_form_submit';
      $form['actions']['submit']['#submit'][] = '_npaa_node_form_submit_redirect';
      // Build form fieldset.
      $form['npaa'] = array(
        '#type' => 'fieldset',
        '#title' => t('Preview and Approve'),
        '#group' => 'additional_settings',
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        '#tree' => TRUE,
      );
      $form['npaa']['header'] = array(
        '#markup' => '<p>' . t('Activate and configure preview and approval of node.') . '</p>',
      );
      $form['npaa']['preview_hash'] = array(
        '#type' => 'textfield',
        '#title' => t('Preview hash'),
        '#maxlength' => 6,
        '#default_value' => ! empty($npaa_node) ? $npaa_node->getPreviewHash() : '',
      );
      $form['npaa']['sid'] = array(
        '#type' => 'select',
        '#title' => t('Status'),
        '#options' => $status_options,
        '#default_value' => ! empty($npaa_node) ? $npaa_node->getSid() : 1,
      );
      $form['npaa']['status_update_code'] = array(
        '#type' => 'textfield',
        '#title' => t('Update code'),
        '#maxlength' => 60,
        '#default_value' => ! empty($npaa_node) ? $npaa_node->getStatusUpdateCode() : '',
      );
      $form['npaa']['send_preview_mail'] = array(
        '#type' => 'checkbox',
        '#title' => t('Send preview mail'),
        '#description' => t('If checked, you will be redirected to a form where you can prepare and send an email with a preview link after that you have updated this content.'),
        '#default_value' => $npaa_settings['node_types'][$type]['default_preview_mail_send_by_default'],
      );
      // Add the option to delete/reset if an NpaaNode already exist.
      if ($npaa_node) {
        $form['npaa']['delete'] = array(
          '#type' => 'checkbox',
          '#title' => t('Remove preview'),
          '#description' => t('Reset the preview functionality for this node.'),
          '#default_value' => 0,
        );
        $form['npaa']['nid'] = array(
          '#type' => 'value',
          '#value' => $npaa_node->getId(),
        );
      }
    }
  }
}

/**
 * FAPI form submit callback for the NPAA section of a node add/edit form.
 *
 * @return void
 * @see npaa_form_alter()
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function _npaa_node_form_submit($form, &$form_state) {
  $form_values = $form_state['values'];
  if (! empty($form_values['npaa']['preview_hash'])) {
    $npaa = $form_values['npaa'];
    $npaa_node = false;
    if (! empty($npaa['nid']) && ($npaa_node = npaa_load_node(intval($npaa['nid'])))) {
      $npaa_save_new = FALSE;
      // Check if it should be deleted.
      if (! empty($npaa['delete']) && $npaa['delete'] === 1 && $npaa_node) {
        npaa_delete_node($npaa_node);
      }
    } elseif (isset($form_values['nid']) && intval($form_values['nid'])) {
      $npaa_node = new NpaaNode();
      $npaa_node->setId(intval($form_values['nid']));
      $npaa_save_new = TRUE;
    }
    if ($npaa_node) {
      $npaa_node
        ->setPreviewHash($npaa['preview_hash'])
        ->setStatusUpdateCode($npaa['status_update_code'])
        ->setSid($npaa['sid']);
      // Save
      if ($npaa_save_new === TRUE) {
        npaa_create_node($npaa_node);
      } else {
        npaa_update_node($npaa_node);
      }
    }
  }
}

/**
 * FAPI form submit callback for the NPAA section of a node add/edit form.
 * Used for redirecting to the preview mail form.
 *
 * @return void
 * @see npaa_form_alter()
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function _npaa_node_form_submit_redirect($form, &$form_state) {
  $form_values = $form_state['values'];
  if (! empty($form_values['npaa']['send_preview_mail'])) {
    $form_state['redirect'] = "node/{$form_values['nid']}/npaa-preview-mail";
  }
}

/**
 * Form function/callback for the node approve form.
 *
 * @return Array
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_node_approve_form($form, &$form_state, $node) {  
  $npaa_statuses = npaa_load_all_statuses();
  $npaa_node = npaa_load_node($node->nid);
  $status_options = array();
  foreach ($npaa_statuses as $npaa_status) {
    if ($npaa_status->getPubliclySelectable()) {
      $status_options[$npaa_status->getId()] = $npaa_status->getTitle();
    }
  }
  $form['#attributes'] = array(
    'class' => array('npaa-form', 'npaa-form-approve-form'),
  );
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );
  $form['sid'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#options' => $status_options,
    '#default_value' => $npaa_node ? $npaa_node->getSid() : 0,
  );
  $form['status_update_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Update code'),
    '#description' => t('Enter the code you recieved to update the status. If you have not been given a code, please contact the person who gave you the preview link.'),
  );
  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update content'),
    '#attributes' => array(
      'class' => array('npaa-btn'),
    ),
  );
  return $form;
}

/**
 * Validate function to the node approve form.
 *
 * @return void
 * @see npaa_node_approve_form_submit()
 * @see npaa_node_approve_form()
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_node_approve_form_validate($form, &$form_state) {
  $form_values = $form_state['values'];
  $npaa_node = npaa_load_node($form_values['nid']);
  if (! $npaa_node) {
    form_set_error('', t('Could not find any preview for this node.'));
  }
  if ($npaa_node->getStatusUpdateCode() !== $form_values['status_update_code']) {
    form_set_error('status_update_code', t('Invalid code.'));
  }
}

/**
 * Submit function to the node approve form.
 *
 * @return void
 * @see npaa_node_approve_form_validate()
 * @see npaa_node_approve_form()
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_node_approve_form_submit($form, &$form_state) {
  $form_values = $form_state['values'];
  $npaa_settings = npaa_settings_get();
  $nid = intval($form_values['nid']);
  $sid = intval($form_values['sid']);
  $npaa_node = npaa_load_node($nid);
  $node = node_load($nid);
  if (in_array($sid, $npaa_settings['statuses']['publish_on_update']) && $node) {
    // Publish node.
    $node->status = 1;
    node_save($node);
    // Check if preview should be deleted on node publish.
    if ($npaa_settings['statuses']['delete_preview_on_publish']) {
      npaa_delete_node($npaa_node);
    } else {
      $npaa_node->setSid($sid);
      npaa_update_node($npaa_node);
    }
    drupal_set_message(t('%title has now been published.', array('%title' => $node->title)));
    entity_get_controller('node')->resetCache(array($node->nid));
    cache_clear_all();
    drupal_goto("node/$nid");
  } else {
    $npaa_node->setSid($sid);
    npaa_update_node($npaa_node);
    drupal_set_message(t('%title has been successfully updated.', array('%title' => $node->title)));
  }
}

/**
 * Form callback for npaa node delete.
 *
 * @return void
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_node_delete_form($form, &$form_state, $node) {
  $form['header'] = array(
    '#markup' => '<h2>' . t('Delete preview for %title', array('%title' => $node->title)) . '</h2>',
  );
  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => intval($node->nid),
  );
  $form['confirm'] = array(
    '#type' => 'checkbox',
    '#title' => t("Yes, I'm sure I want to delete the preview."),
    '#default_value' => 0,
  );
  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete preview'),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'button',
    '#value' => t('Cancel'),
  );
  return $form;
}

/**
 * Form validate callback for npaa node delete.
 *
 * @return void
 * @see npaa_node_delete_form()
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_node_delete_form_validate($form, &$form_state) {
  $values = &$form_state['values'];
  if ($values['op'] === t('Cancel')) {
    drupal_goto('admin/content/npaa');
  }
  if (! $values['confirm']) {
    form_error($form['confirm'], t('You need to confirm that you want to delete the preview.'));
  }
  // Make sure the preview actually exist.
  $values['npaa_node'] = npaa_load_node($values['nid']);
  if (! $values['npaa_node']) {
    form_error($form['nid'], t('The preview does not exist.'));
  }
}

/**
 * Form submit callback for npaa node delete.
 *
 * @return void
 * @see npaa_node_delete_form()
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_node_delete_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $result = npaa_delete_node($values['npaa_node']);
  if ($result) {
    drupal_set_message(t('Successfully deleted preview.'));
  } else {
    drupal_set_message(t('Could not delete preview.'), 'error');
  }
  drupal_goto('admin/content/npaa');
}

/**
 * Form function/callback for the node preview mail form.
 * 
 * Form to send an email with a preview link and information.
 *
 * @return array
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_node_preview_mail_form($form, &$form_state, $node) {
  global $language;
  $path = drupal_get_path('module', 'npaa');
  $npaa_node = npaa_load_node($node->nid);
  $npaa_settings = npaa_settings_get();
  $preview_mail_settings = $npaa_settings['node_types'][$node->type]['default_preview_mail'];
  $default_text_format = filter_default_format();

  $form['#attached'] = array(
    'css' => array(
      $path . '/assets/css/npaa.admin.css',
    ),
    'js' => array(
      $path . '/assets/js/build/npaa.admin.js',
    ),
  );
  $form['header'] = array(
    '#markup' => '<h2>' . t('Send a preview email for %title', array('%title' => $node->title)) . '</h2>',
  );
  $form['tokens'] = array(
    '#theme' => 'item_list',
    '#items' => array(
      '<span class="npaa-admin-box__link-description">' . t('ID') . ':</span> <a href="#" class="npaa-copy-paste-field">' . $npaa_node->getId() . '</a>',
      '<span class="npaa-admin-box__link-description">' . t('Update code') . ':</span> <a href="#" class="npaa-copy-paste-field">' . $npaa_node->getStatusUpdateCode() . '</a>',
      '<span class="npaa-admin-box__link-description">' . t('Preview hash') . ':</span> <a href="#" class="npaa-copy-paste-field">' . $npaa_node->getPreviewHash() . '</a>',
      '<span class="npaa-admin-box__link-description">' . t('Preview URL') . ':</span> <a href="#" class="npaa-copy-paste-field">' . npaa_preview_url($node->nid, true, false) . '</a>',
    ),
    '#prefix' => '<div class="npaa-admin-box"><h4>' . t('Usuable preview fields') . '</h4>',
    '#suffix' => '<p class="description">' . t("Click on one of the values to insert it to where you'r marker is currently at.") . '</p></div>',
  );
  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => intval($node->nid),
  );
  $form['to'] = array(
    '#type' => 'textfield',
    '#title' => t('To'),
    '#description' => t('The address the message will be sent to.'),
    '#default_value' => '',
  );
  $form['from'] = array(
    '#type' => 'textfield',
    '#title' => t('From'),
    '#description' => t('The address the message will be marked as being from.'),
    '#default_value' => isset($preview_mail_settings['from']) ? _npaa_mail_token_replace($preview_mail_settings['from'], array('node' => $node), $language) : _npaa_mail_text_default('default_preview_mail_from', $language, array('node' => $node)),
  );
  $form['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#description' => t('The subject of the email.'),
    '#default_value' => isset($preview_mail_settings['subject']) ? _npaa_mail_token_replace($preview_mail_settings['subject'], array('node' => $node), $language) : _npaa_mail_text_default('default_preview_mail_subject', $language, array('node' => $node)),
  );
  $form['body'] = array(
    '#type' => 'text_format',
    '#title' => t('Body'),
    '#description' => t('The body of the email.'),
    '#base_type' => 'textarea',
    '#default_value' => isset($preview_mail_settings['body']['value']) ? _npaa_mail_token_replace($preview_mail_settings['body']['value'], array('node' => $node), $language) : _npaa_mail_text_default('default_preview_mail_body', $language, array('node' => $node)),
    '#format' => isset($preview_mail_settings['body']['format']) ? $preview_mail_settings['body']['format'] : $default_text_format,
  );
  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send email'),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'button',
    '#value' => t('Cancel'),
  );
  return $form;
}

/**
 * Form validate callback for the preview mail form.
 *
 * @return void
 * @see npaa_node_preview_mail_form()
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_node_preview_mail_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['op'] === t('Cancel')) {
    drupal_goto('admin/content/npaa');
  }
  if (! $values['to'] || ! valid_email_address($values['to'])) {
    form_error($form['to'], t('You need to add a valid email address which the email will be sent to.'));
  }
  if (! $values['from'] || ! valid_email_address($values['from'])) {
    form_error($form['from'], t('You need to add a valid email address which the email will be sent from.'));
  }
  if (! trim($values['body']['value'])) {
    form_error($form['body'], t('You need to enter a text for the email.'));
  }
}

/**
 * Form submit callback for the preview mail form.
 *
 * @return void
 * @see npaa_node_preview_mail_form()
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_node_preview_mail_form_submit($form, &$form_state) {
  global $language;
  $values = $form_state['values'];
  $result = drupal_mail('npaa', 'npaa_preview_mail', $values['to'], $language, $values, $values['from'], true);
  if ($result) {
    drupal_set_message(t('The preview email was sent.'));
  } else {
    drupal_set_message(t('There were problems sending the preview email.'), 'error');
  }
}

/**
 * Module settings form.
 *
 * @return Array
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_settings_form($form, &$form_state) {
  global $language;
  $npaa_settings = npaa_settings_get();
  $npaa_statuses = npaa_load_all_statuses();
  $node_types = node_type_get_types();
  $default_text_format = filter_default_format();

  $status_options = array();
  foreach ($npaa_statuses as $npaa_status) {
    $status_options[(string) $npaa_status->getId()] = $npaa_status->getTitle();
  }

  $form['#theme'] = 'system_settings_form';

  $form['approve_link_in_drupal_messages'] = array(
    '#type' => 'checkbox',
    '#title' => t('Print approve link in drupal messages.'),
    '#default_value' => isset($npaa_settings['approve_link_in_drupal_messages']) ? $npaa_settings['approve_link_in_drupal_messages'] : 1,
    '#description' => t('The approve link will be printed with <em>drupal_set_message()</em>. The approve link can also be printed with a panels content pane or through the api function <em>npaa_approve_link()</em>.'),
  );

  $form['statuses'] = array(
    '#type' => 'fieldset',
    '#title' => t('Statuses'),
    '#collapsible' => TRUE,
    '#tree' => TRUE,
  );

  $form['statuses']['publish_on_update'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Publish on update'),
    '#description' => t('Will publish the node when it is updated with one of the following selected statuses.'),
    '#options' => $status_options,
    '#default_value' => $npaa_settings['statuses']['publish_on_update'],
  );
  $form['statuses']['delete_preview_on_publish'] = array(
    '#type' => 'checkbox',
    '#title' => t('Delete preview on publish'),
    '#description' => t('Delete <em>the preview</em> when the node is being published due to the setting above. This will only remove the preview part of the node and not the node itself.'),
    '#default_value' => $npaa_settings['statuses']['delete_preview_on_publish'],
  );

  $form['node_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node types'),
    '#description' => t('Node type specific settings.'),
    '#collapsible' => TRUE,
    '#tree' => TRUE,
  );

  foreach ($node_types as $type => $data) {
    $type_settings = isset($npaa_settings['node_types'][$type]) ? $npaa_settings['node_types'][$type] : array();
    // Used to hide unrelated 
    $visibility_condition = array(
      '#states' => array(
        'visible' => array(
          '#edit-node-types-' . $type . '-activated' => array('checked' => TRUE),
        ),
      ),
    );

    $form['node_types'][$type] = array(
      '#type' => 'fieldset',
      '#title' => t($data->name),
      '#description' => t($data->description),
      '#collapsible' => TRUE,
    );
    $form['node_types'][$type]['activated'] = array(
      '#type' => 'checkbox',
      '#title' => t('Activated'),
      '#default_value' => isset($npaa_settings['node_types'][$type]['activated']) ? $npaa_settings['node_types'][$type]['activated'] : 0,
      '#description' => t('Determines if the preview function should be activated for node type %type.', array('%type' => $type)),
    );
    $form['node_types'][$type]['default_preview_mail_send_by_default'] = array(
      '#type' => 'checkbox',
      '#title' => t('Send preview mail by default'),
      '#default_value' => isset($npaa_settings['node_types'][$type]['default_preview_mail_send_by_default']) ? $npaa_settings['node_types'][$type]['default_preview_mail_send_by_default'] : 0,
      '#description' => t('Determines if an email should be sent by default for the node type %type. Can always be overriden for each single node.', array('%type' => $type)),
    ) + $visibility_condition;
    $form['node_types'][$type]['default_preview_mail'] = array(
      '#type' => 'fieldset',
      '#title' => t('Default preview mail settings'),
      '#description' => t('These settings will be used by default but can be overridden for each mail sent.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ) + $visibility_condition;
    $form['node_types'][$type]['default_preview_mail']['from'] = array(
      '#type' => 'textfield',
      '#title' => t('From'),
      '#description' => t('The address the message will be marked as being from.'),
      '#default_value' => isset($type_settings['default_preview_mail']['from']) ? $type_settings['default_preview_mail']['from'] : _npaa_mail_text_default('default_preview_mail_from', $language),
    );
    $form['node_types'][$type]['default_preview_mail']['subject'] = array(
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#description' => t('The subject of the email.'),
      '#default_value' => isset($type_settings['default_preview_mail']['subject']) ? $type_settings['default_preview_mail']['subject'] : _npaa_mail_text_default('default_preview_mail_subject', $language),
    );
    $form['node_types'][$type]['default_preview_mail']['body'] = array(
      '#type' => 'text_format',
      '#title' => t('Body'),
      '#description' => t('The body of the email.'),
      '#base_type' => 'textarea',
      '#default_value' => isset($type_settings['default_preview_mail']['body']['value']) ? $type_settings['default_preview_mail']['body']['value'] : _npaa_mail_text_default('default_preview_mail_body', $language),
      '#format' => isset($type_settings['default_preview_mail']['body']['format']) ? $type_settings['default_preview_mail']['body']['format'] : $default_text_format,
    );
    $form['node_types'][$type]['default_preview_mail']['tokens'] = array(
      '#theme' => 'token_tree_link',
      // To get these options to work, use the patch from https://www.drupal.org/node/2289203 if you have Token 7.x-1.5 or earlier.
      '#token_types' => array('node'), // The token types that have specific context. Can be multiple token types like 'term' and/or 'user'
      '#global_types' => TRUE, // A boolean TRUE or FALSE whether to include 'global' context tokens like [current-user:*] or [site:*]. Defaults to TRUE.
      '#click_insert' => TRUE, // A boolean whether to include the 'Click this token to insert in into the the focused textfield' JavaScript functionality. Defaults to TRUE.
    );
  }

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  return $form;
}

/**
 * Form validate callback for the module settings form.
 *
 * @return void
 * @see npaa_settings_form()
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_settings_form_validate($form, &$form_state) {
}

/**
 * Form submit callback for the module settings form.
 *
 * @return void
 * @see npaa_settings_form()
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_settings_form_submit($form, &$form_state) {
  $form_values = $form_state['values'];
  $settings = array();

  $settings['approve_link_in_drupal_messages'] = $form_values['approve_link_in_drupal_messages'];
  $settings['node_types'] = $form_values['node_types'];
  $settings['statuses'] = $form_values['statuses'];
  $settings['statuses']['publish_on_update'] = array_filter($settings['statuses']['publish_on_update']);

  npaa_settings_set($settings);
}

/**
 * undocumented function
 *
 * @return void
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_statuses_form($form, &$form_state) {
    $form['nodes'] = npaa_status_nodes();
    return $form;
}

/**
 * undocumented function
 *
 * @return void
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_nodes_form($form, &$form_state) {
    $form['nodes'] = npaa_node_nodes();
    return $form;
}

/**
 * undocumented function
 *
 * @return void
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_nodes_add_node_form($form, &$form_state) {
  $npaa_settings = npaa_settings_get();
  $activated_types = array_values($npaa_settings['activated_node_types']);

  if (empty($activated_types)) {
    $form['header'] = array(
      '#markup' => '<div class="messages error">' . t('NPAA has not been activated for any node type.') . '</div><p>' . t('Activate NPAA for node types at the !link.', array('!link' => l(t('NPAA configuration page'), 'admin/config/content/npaa'))) . '</p>',
    );
    return $form;
  }

  if (! user_access('npaa set node preview')) {
    $form['header'] = array(
      '#markup' => '<div class="messages error">' . t('You do not have permission to set node previews.') . '</div>',
    );
    return $form;
  }

  // Get all NPAA Statuses for the select list.
  $npaa_statuses = npaa_load_all_statuses();
  $status_options = array();
  foreach ($npaa_statuses as $npaa_status) {
    $status_options[$npaa_status->getId()] = $npaa_status->getTitle();
  }

  $form['header'] = array(
    '#markup' => '<p>' . t('Activate and configure preview and approval of node.') . '</p>',
  );
  // Select all nodes.
  $query = db_select('node', 'n')
    ->range(0, 100)
    ->orderBy('n.changed', 'DESC')
    ->fields('n', array('nid', 'title', 'language'));
  // Make sure to only select nodes which doesn't already have a preview created.
  $query->leftJoin('npaa_nodes', 'nn', 'n.nid = nn.nid');
  $query->condition('nn.nid', NULL);
  // Execute select query.
  $nodes = $query->execute()->fetchAllAssoc('nid', PDO::FETCH_ASSOC);
  // Fix FAPI options array.
  foreach ($nodes as $key => &$value) {
    $value = sprintf('%s (%s)', $value['title'], strtoupper($value['language']));
  }

  $form['nid'] = array(
    '#type' => 'select',
    '#title' => t('Node'),
    '#options' => $nodes,
    '#default_value' => '',
  );
  $form['preview_hash'] = array(
    '#type' => 'textfield',
    '#title' => t('Preview hash'),
    '#maxlength' => 6,
    '#default_value' => '',
  );
  $form['sid'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#options' => $status_options,
    '#default_value' => '',
  );
  $form['status_update_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Update code'),
    '#description' => t('A code which the user uses to update the status.'),
    '#maxlength' => 60,
    '#default_value' => '',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  return $form;
}

/**
 * undocumented function
 *
 * @return void
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_nodes_add_node_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $node = new NpaaNode();
  $node
    ->setId($values['nid'])
    ->setPreviewHash($values['preview_hash'])
    ->setSid($values['sid'])
    ->setStatusUpdateCode($values['status_update_code']);
  $res = npaa_create_node($node);
  if ($res === TRUE) {
    drupal_goto('admin/content/npaa');
  }
}

/**
 * undocumented function
 *
 * @return void
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_status_nodes() {
  $form = array('nodes' => array());

  $header = array(
    'title' => array('data' => t('Title'), 'field' => 'ns.title'),
    'description' => array('data' => t('Description'), 'field' => 'ns.description'),
    'publicly_selectable' => array('data' => t('Publicly selectable'), 'field' => 'ns.publicly_selectable'),
    'operations' => array('data' => t('Operations')),
  );

  $query = db_select('npaa_statuses', 'ns')
    ->extend('TableSort');

  $query
    ->fields('ns')
    ->orderByHeader($header);

  $statuses = $query->execute()->fetchAllAssoc('sid', PDO::FETCH_ASSOC);

  $options = array();
  foreach ($statuses as $status) {
    $operation_links = array();

    if ($status['sid'] > 3) {
      $operation_links[] = array('title' => t('Edit'), 'href' => '#');
      $operation_links[] = array('title' => t('Delete'), 'href' => '#');
    }

    $options[] = array(
      'title' => $status['title'],
      'description' => $status['description'],
      'publicly_selectable' => $status['publicly_selectable'] ? t('Yes') : t('No'),
      'operations' => theme('links', array(
        'links' => $operation_links,
        'attributes' => array('class' => 'links inline'),
      )),
    );
  }

  $form['nodes']['#markup'] = theme('table', array(
    'header' => $header,
    'rows' => $options,
  ));

  return $form;
}

/**
 * undocumented function
 *
 * @return void
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_node_nodes() {
  $form = array();
  $node_types = node_type_get_types();
  $languages = language_list();

  $header = array(
    'title' => array('data' => t('Title'), 'field' => 'n.title'),
    'type' => array('data' => t('Type'), 'field' => 'n.type'),
    'author' => t('Author'),
    'published' => array('data' => t('Published'), 'field' => 'n.status'),
    'preview_hash' => array('data' => t('Preview hash'), 'field' => 'nn.preview_hash'),
    'status' => array('data' => t('Status'), 'field' => 'ns.title'),
    'status_update_code' => array('data' => t('Update code'), 'field' => 'nn.status_update_code'),
    'operations' => array('data' => t('Operations')),
  );

  $query = db_select('npaa_nodes', 'nn')
    ->extend('PagerDefault')
    ->extend('TableSort')
    ->limit(50)
    ->orderByHeader($header)
    ->fields('nn')
    ->fields('n', array('type'));

  $query->join('node', 'n', 'n.nid = nn.nid');
  $query->join('users', 'u', 'n.uid = u.uid');
  $query->join('npaa_statuses', 'ns', 'ns.sid = nn.sid');
  $query->addField('n', 'title', 'title');
  $query->addField('n', 'status', 'published');
  $query->addField('n', 'language', 'language');
  $query->addField('n', 'changed', 'changed');
  $query->addField('u', 'name', 'author');
  $query->addField('ns', 'title', 'status');

  $nodes = $query->execute()->fetchAllAssoc('nid', PDO::FETCH_ASSOC);

  $options = array();
  foreach ($nodes as $node) {
    $l_options = $node['language'] != LANGUAGE_NONE && isset($languages[$node['language']]) ? array('language' => $languages[$node['language']]) : array();
    $node['published'] = $node['published'] ? t('published') : t('not published');
    $node['title'] = array(
      'data' => array(
        '#type' => 'link',
        '#title' => $node['title'],
        '#href' => 'node/' . $node['nid'],
        '#options' => $l_options,
        '#suffix' => ' ' . theme('mark', array('type' => node_mark($node['nid'], $node['changed']))),
      ),
    );
    $node['preview_hash'] = array(
      'data' => array(
        '#type' => 'link',
        '#title' => $node['preview_hash'],
        '#href' => 'node/' . $node['nid'],
        '#options' => array_merge($l_options, array('query' => array('preview' => $node['preview_hash']))),
      ),
    );
    // Node operations
    $operations = array();
    $operations['edit'] = array(
      'title' => t('edit'),
      'href' => 'node/' . $node['nid'] . '/edit',
      'fragment' => 'edit-npaa',
    );
    $operations['preview-email'] = array(
      'title' => t('preview email'),
      'href' => 'node/' . $node['nid'] . '/npaa-preview-mail',
      'fragment' => '',
    );
    $operations['delete'] = array(
      'title' => t('delete'),
      'href' => 'node/' . $node['nid'] . '/npaa-delete',
      'fragment' => '',
    );
    if (count($operations) > 1) {
      $node['operations'] = array(
        'data' => array(
          '#theme' => 'links__node_operations',
          '#links' => $operations,
          '#attributes' => array('class' => array('links', 'inline')),
        ),
      );
    } elseif (!empty($operations)) {
      $link = reset($operations);
      $node['operations'] = array(
        'data' => array(
          '#type' => 'link',
          '#title' => $link['title'],
          '#href' => $link['href'],
          '#fragment' => $link['fragment'],
        ),
      );
    }
    $options[$node['nid']] = $node;
  }

  if (! empty($options)) {
    $npaa_admin_access = user_access('npaa administration');
    // Build the 'Update options' form.
    $form['options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Update options'),
      '#attributes' => array('class' => array('container-inline')),
      '#access' => $npaa_admin_access,
    );
    $operation_options = array();
    foreach (module_invoke_all('npaa_operations') as $operation => $array) {
      $operation_options[$operation] = $array['label'];
    }
    $form['options']['operation'] = array(
      '#type' => 'select',
      '#title' => t('Operation'),
      '#title_display' => 'invisible',
      '#options' => $operation_options,
      '#default_value' => 'approve',
    );
    $form['options']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
      '#validate' => array('npaa_nodes_validate'),
      '#submit' => array('npaa_nodes_submit'),
    );
  }
  $form['nodes'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No content available.'),
  );
  return $form;
}

/**
 * Applies filters for node administration filters based on session.
 *
 * @param $query
 *   A SelectQuery to which the filters should be applied.
 * 
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 */
function npaa_build_filter_query(SelectQueryInterface $query) {
  // Build query
  $filter_data = isset($_SESSION['node_overview_filter']) ? $_SESSION['node_overview_filter'] : array();
  foreach ($filter_data as $index => $filter) {
    list($key, $value) = $filter;
    switch ($key) {
      case 'status':
        // Note: no exploitable hole as $key/$value have already been checked when submitted
        list($key, $value) = explode('-', $value, 2);
      case 'type':
      case 'language':
        $query->condition('n.' . $key, $value);
        break;
    }
  }
}

/**
 * undocumented function
 *
 * @return void
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_nodes_validate($form, &$form_state) {
  // Error if there are no items to select.
  if (!is_array($form_state['values']['nodes']) || !count(array_filter($form_state['values']['nodes']))) {
    form_set_error('', t('No items selected.'));
  }
}

/**
 * Process node_admin_nodes form submissions.
 *
 * Executes the chosen 'Update option' on the selected nodes.
 *
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_nodes_submit($form, &$form_state) {
  $operations = module_invoke_all('npaa_operations');
  $operation = $operations[$form_state['values']['operation']];
  // Filter out unchecked nodes
  $nodes = array_filter($form_state['values']['nodes']);
  if ($function = $operation['callback']) {
    // Add in callback arguments if present.
    if (isset($operation['callback arguments'])) {
      $args = array_merge(array($nodes), $operation['callback arguments']);
    }
    else {
      $args = array($nodes);
    }
    call_user_func_array($function, $args);

    cache_clear_all();
  }
  else {
    // We need to rebuild the form to go to a second step. For example, to
    // show the confirmation form for the deletion of nodes.
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * undocumented function
 *
 * @return void
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_npaa_operations() {
  $operations = array(
    'update hashes' => array(
      'label' => t('Update selected content with new random preview hashes'),
      'callback' => 'npaa_mass_update_hashes',
      'callback arguments' => array('preview_hash'),
    ),
    'update update code' => array(
      'label' => t('Update selected content with new random update codes'),
      'callback' => 'npaa_mass_update_hashes',
      'callback arguments' => array('status_update_code', 4),
    ),
    'delete' => array(
      'label' => t('Delete preview from selected content'),
      'callback' => 'npaa_mass_delete_preview',
    ),
  );
  return $operations;
}

/**
 * undocumented function
 *
 * @return void
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_mass_update_hashes($nodes, $hash_field, $length = 6) {
  if (! in_array($hash_field, array('preview_hash', 'status_update_code'))) {
    drupal_set_message(t('Invalid field'), 'error');
    return;
  }
  $updates = 0;
  foreach ($nodes as $nid) {
    $res = db_update('npaa_nodes')
      ->fields(array(
        $hash_field => _npaa_random_string($length),
      ))
      ->condition('nid', $nid)
      ->execute();
    if ($res) {
      $updates++;
    }
  }
  if ($updates === count($nodes)) {
    drupal_set_message(t('Successfully updated @updates.', array('@updates' => format_plural($updates, 'one node', '@count nodes'))), 'status');
  } else {
    drupal_set_message(t('Could only update @updated out of @total nodes.', array('@updated' => $updates, '@total' => count($nodes))), 'error');
  }
}

/**
 * undocumented function
 *
 * @return void
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_mass_delete_preview($nodes) {
  $num_deleted = db_delete('npaa_nodes')
    ->condition('nid', array_values($nodes), 'IN')
    ->execute();
  if ($num_deleted === count($nodes)) {
    drupal_set_message(t('Successfully deleted @deletes.', array('@deletes' => format_plural($num_deleted, 'one preview', '@count previews'))), 'status');
  } else {
    drupal_set_message(t('Could only delete @deletes out of @total previews.', array('@deletes' => $num_deleted, '@total' => count($nodes))), 'error');
  }
}

/**
 * undocumented function
 *
 * @return void
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function _npaa_random_string($length = 6) {
  $str = '';
  $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  $max = strlen($characters) - 1;
  for ($i = 0; $i < $length; $i++) {
    $rand = mt_rand(0, $max);
    $str .= $characters[$rand];
  }
  return $str;
}
