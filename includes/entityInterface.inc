<?php

/**
 * @file
 * Contains the NpaaEntityInterface.
 */

/**
 * Interface for the Npaa Entity.
 *
 * @package default
 * @author 
 **/
interface NpaaEntityInterface
{
  /**
   * Constructor.
   *
   * @return void
   * @author 
   **/
  public function __construct();

  /**
   * Returns all fields as they are named in the db schema.
   *
   * @return array
   * @author 
   **/
  public function fields();
} // END interface NpaaEntityInterface
