<?php

/**
 * @file
 * Contains the NpaaNode class.
 */

/**
 * Class for the NPAA Node.
 *
 * @package default
 * @author 
 **/
class NpaaNode implements NpaaEntityInterface
{
  private $_id = 0;

  private $_preview_hash = '';

  private $_sid = 0;

  private $_status_update_code = '';

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function __construct() {
    return $this;
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function getId() {
    return intval($this->_id);
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function setId($id) {
    $this->_id = intval($id);
    return $this;
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function getPreviewHash() {
    return trim($this->_preview_hash);
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function setPreviewHash($preview_hash) {
    $this->_preview_hash = trim($preview_hash);
    return $this;
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function getSid() {
    return intval($this->_sid);
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function setSid($sid) {
    $this->_sid = intval($sid);
    return $this;
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function getStatusUpdateCode() {
    return trim($this->_status_update_code);
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function setStatusUpdateCode($status_update_code) {
    $this->_status_update_code = trim($status_update_code);
    return $this;
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function fields() {
    return array(
      'nid' => $this->getId(),
      'preview_hash' => $this->getPreviewHash(),
      'sid' => $this->getSid(),
      'status_update_code' => $this->getStatusUpdateCode(),
    );
  }
} // END class NpaaNode
