<?php

/**
 * @file
 * Defines additional tokens specific for Npaa.
 */

/**
 * Implements hook_token_info_alter().
 *
 * @return void
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_token_info_alter(&$data) {
  // Types
  $data['types']['npaa-preview'] = array(
    'name' => t('Npaa Previews'),
    'description' => t('Tokens related to individual Npaa previews.'),
    'needs-data' => 'node',
  );
  $data['types']['npaa-status'] = array(
    'name' => t('Npaa statuses'),
    'description' => t('Tokens related to individual Npaa statuses.'),
    'needs-data' => 'npaa-preview',
  );
  // Add token as child to node.
  $data['tokens']['node']['npaa-preview'] = array(
    'name' => t('Node preview'),
    'description' => t('Preview of a node.'),
    'type' => 'npaa-preview',
  );
  // Add tokens for Previews
  $data['tokens']['npaa-preview']['url'] = array(
    'name' => t('Preview URL'),
    'description' => t('The full URL to the preview of the node.'),
  );
  $data['tokens']['npaa-preview']['preview-hash'] = array(
    'name' => t('Preview hash'),
    'description' => t('String used for accessing the node preview.'),
  );
  $data['tokens']['npaa-preview']['update-code'] = array(
    'name' => t('Update code'),
    'description' => t('Code used for updating the preview status of the node.'),
  );
  // Add token as child to Preview.
  $data['tokens']['npaa-preview']['status'] = array(
    'name' => t('Preview status'),
    'description' => t("Status of the node's preview."),
    'type' => 'npaa-status',
  );
  // Add tokens for preview statuses.
  $data['tokens']['npaa-status']['id'] = array(
    'name' => t('Status ID'),
    'description' => t("The preview's status ID."),
  );
  $data['tokens']['npaa-status']['title'] = array(
    'name' => t('Status title'),
    'description' => t("Status title."),
  );
  $data['tokens']['npaa-status']['description'] = array(
    'name' => t('Status description'),
    'description' => t("Status description."),
  );
}

/**
 * Implements hook_tokens().
 *
 * @return array
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function npaa_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  $node = false;
  $npaa_node = false;  
  $npaa_status = false;
  // Make sure at least one Npaa token exist. No reason to continue otherwise.
  if (! _npaa_tokens_contain_npaa_token($tokens)) {
    return $replacements;
  }
  if ($type === 'node' && ! empty($data['node'])) {
    $node = $data['node'];
    $npaa_node = npaa_load_node($node->nid);
  }
  if ($npaa_node) {
    $npaa_status = npaa_load_status($npaa_node->getSid());
    foreach ($tokens as $name => $original) {
      switch ($name) {
        // All preview related tokens
        case 'npaa-preview':
        case 'npaa-preview:url':
          $replacements[$original] = npaa_preview_url($node->nid, true, false);
          break;
        case 'npaa-preview:update-url':
          $replacements[$original] = npaa_approve_url($node->nid, true, false);
          break;
        case 'npaa-preview:preview-hash':
          $replacements[$original] = $npaa_node->getPreviewHash();
          break;
        case 'npaa-preview:update-code':
          $replacements[$original] = $npaa_node->getStatusUpdateCode();
          break;
        // All status related stuff below.
        case 'npaa-preview:status':
        case 'npaa-preview:status:title':
          $replacements[$original] = $npaa_status->getTitle();
          break;
        case 'npaa-preview:status:id':
          $replacements[$original] = $npaa_status->getId();
          break;
        case 'npaa-preview:status:description':
          $replacements[$original] = $npaa_status->getDescription();
          break;
      }
    }
  }
  return $replacements;
}

/**
 * Helper function which checks if an Npaa token exist.
 *
 * @return boolean
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function _npaa_tokens_contain_npaa_token(Array $tokens) {
  foreach ($tokens as $token) {
    if (strpos($token, 'npaa-preview') !== false) {
      return true;
    }
  }
  return false;
}
